#ifndef TITANIUM_DRIVER_BOARDIMPL_HPP_
#define TITANIUM_DRIVER_BOARDIMPL_HPP_

#include <GPIO.hpp>
#include <I2C.hpp>
#include <Pin.hpp>
#include <Board.hpp>
#include <LedRGB/LedRGB.hpp>
#include <CAT24/CAT24M01.hpp>

namespace Titanium::Board {

  using namespace Titanium::Drivers;
  using namespace Titanium::Devices;

  class cBoardAxiomS: public cBoard {
    private:
      //cGPIO *led_r;
      //cGPIO *led_g;
      //cGPIO *led_b;
      //cGPIOConfig gc;
      cLedRGB* led_rgb;
      cCAT24M01* eeprom;

      cCore* newCore(void) override;
      void createComponents(void) override;

    public:
      cBoardAxiomS(void);

      cLedRGB* getLedRGB(void) const;
      cCAT24M01* getCAT24M01(void) const;
  };

  inline cLedRGB* cBoardAxiomS::getLedRGB(void) const{
    return led_rgb;
  }

  inline cCAT24M01* cBoardAxiomS::getCAT24M01(void) const {
    return eeprom;
  }

}

#endif
