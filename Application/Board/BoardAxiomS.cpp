#include <BoardAxiomS.hpp>
#include <LedRGB/LedRGB.hpp>
#include "CoreImpl.hpp"

namespace Titanium::Board {

  cBoardAxiomS::cBoardAxiomS(void){
  }

  cCore* cBoardAxiomS::newCore(void){
    return new cCoreImpl();
  }

  void cBoardAxiomS::createComponents(void){
    //led_r = CreateGPIO( getPin( ePortName::D, 3 ), cGPIOConfig::defaultOutput );
    //led_g = CreateGPIO( getPin( ePortName::D, 4 ), cGPIOConfig::defaultOutput );
    //led_b = CreateGPIO( getPin( ePortName::D, 5 ), cGPIOConfig::defaultOutput );
    //led_rgb = new cLedRGB( led_r, led_g, led_b );
    led_rgb = new cLedRGB(ePortName::D, 3, ePortName::D, 4, ePortName::D, 5);
    cBoard &board = getBoard();
    eeprom = new cCAT24M01( cCAT24::cCAT24::ADDR_0, eI2C::I2C_4, board.getPin( ePortName::D, 13 ), board.getPin( ePortName::D, 12 ) );
  }

}
