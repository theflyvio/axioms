/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : tAxiomS.cpp
 * @brief         : Program body.
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

/*
 * #################################################################################################
 * Build directives
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Includes
 * #################################################################################################
 */

#include <BoardAxiomS.hpp>
#include "AxiomS.hpp"

#include "CoreImpl.hpp"
//#include <stm32h7xx.h>

/*
 * #################################################################################################
 * Data types definition
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Namespace ???
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Defines
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Data instance
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Local method prototypes
 * #################################################################################################
 */

/*
 * #################################################################################################
 * External method definitions
 * #################################################################################################
 */

using namespace Titanium::db;

cAxiomS_t::cAxiomS_t(void)
    : cTitaniumLeader(){
}

cAxiomS_t::~cAxiomS_t(){
}

cBoard* cAxiomS_t::createBoard(void){
  return new cBoardAxiomS();
}

void cAxiomS_t::createLocalRegisters(void){
  cUserProfile_t BASADV(eUserProfile_t::BAS|eUserProfile_t::ADV);
  cUserProfile_t ENG(eUserProfile_t::ENG);
  add(l1 = cRegLocalPrcU08Generic(1, 0x3333, eRegisterPersistence_t::NEVER, 0, ENG, ENG));
  add(l2 = cRegLocalCfgU08Ranged(1, 0x4444, eRegisterPersistence_t::NEVER, 0, BASADV, ENG, 10, 100));
  add(userid = cRegLocalCfgUserid(0xABCD));
  add(username = cRegLocalCfgUsername("unknown"));
  add(one = cRegLocalPrcOne(1, 0x3333, eRegisterPersistence_t::NEVER, 0, ENG, ENG));
}

void cAxiomS_t::createRemoteRegisters(void){
  add(r1 = cRegRemotePrcU08Generic(1, 0x5555, eRegisterPersistence_t::NEVER, 0));
  add(r2 = cRegRemoteCfgU08Ranged(1, 0x6666, eRegisterPersistence_t::NEVER, 0));
  add(rem_userid = cRegRemoteCfgUserid());
  add(rem_username = cRegRemoteCfgUsername("undefined"));
}

void cAxiomS_t::createAppTasks(void){
  //addTasks(new TMetrologic);
  //addTasks(new ...);
//  cUser_t u;
//  u.setProfile( cUserProfile_t( eUserProfile_t::ENG ) );
//  if( u.allowWrite( { l1, l2 } ) )
//  {
//    l1->set( 15 );
//    l2->set( 20 );
//    l1->set( sizeof(cAppKey) );
//    l1->set( sizeof(cRegister) );
//    l1->set( sizeof(cRegisterFormat_t) );
//    l1->set( sizeof(cRegisterU08_t) );
//    l1->set( sizeof(cRegisterOne_t) );
//    l1->set( sizeof(cRegisterU08Ranged_t) );
//    l1->set( sizeof(cRegisterUserid_t) );
//    l1->set( sizeof(cRegisterUsername_t) );
//
//    char *s = username->getString(); // perigoso, trabalhar somente com cópia?
//    username->set('N', 1);
//    s = username->getString();
//    s++;
//
//    uint32_t id = userid->getId();
//    userid->set(0x12, 2);
//    id = userid->getId();
//
//   cLedRGB& led = *GetBoardAxiom()->getLedRGB();
//   led.lock(this);
//    led.set(false, false, false);
//    led.set(true, false, false);
//    led.set(false, true, false);
//    led.set(false, false, true);
  //led.set(true, false, true);
  uint8_t color = 0;
  /* while(true){
    color = (color+1)&0x7;
    led.set(this, color);
    uint32_t count = 0;
    while(count++<10000000);
    //led.set(0);
    //HAL_Delay(1000);
  } */
  main_task = new cMainTask{ "Main task", bytePool, 1024UL, 15, 15, 0, TX_AUTO_START, this };
  //}
}

/**
 * *************************************************************************************************
 * @brief   Call RTOS scheduler, it will not return.
 *          main() -> cAxiomS.begin() -> cTitanium.init() -> cAxiomS.startRtosScheduler()
 * *************************************************************************************************
 * 
 * *************************************************************************************************
 */
void cAxiomS_t::startRtosScheduler(void)
{
  /* Call ThreadX tx_kernel_enter() that calls the scheduler */
  tx_kernel_enter();
}

/**
 * *************************************************************************************************
 * @brief   This function is called during RTOS initialization, before calling the scheduler.
 *          It should define all of the initial application threads, queues, semaphores, mutexes, 
 *          event flags, memory pools, and timers. It is also possible to create and delete system
 *          resources from threads during the normal operation of the application. However, all 
 *          initial application resources are defined here.
 * 
 *          After initialization is complete, only an executing thread can create and delete system 
 *          resources, including other threads. Therefore, at least one thread must be created 
 *          during initialization.
 * 
 *          main() -> cAxiomS.begin() -> cTitanium.init() -> cAxiomS.startRtosScheduler() ->
 *          tx_kernel_enter() -> tx_application_define() -> cAxiomS_t::rtosInitCallback()
 * *************************************************************************************************
 * 
 * @param   unusedMemory 
 *            Pointer to free memory available for RTOS usage. Size is defined in linker description
 *            file *.ld
 * 
 * *************************************************************************************************
 */
void cAxiomS_t::rtosInitCallback( void *unusedMemory )
{
  /* Create memory polls */
  bytePool = new cBytePool{ "Main byte memory pool", unusedMemory, 8192UL };
  /* Create application tasks */
  createAppTasks();
}

/*
 * #################################################################################################
 * Local method definitions
 * #################################################################################################
 */
