/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : RgbLedTask.hpp
 * @brief         : Provide ...
 *
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

/*
 * #################################################################################################
 * Define to prevent recursive inclusion
 * #################################################################################################
 */

#ifndef __APP_RGB_LED_TASK_HPP
#define __APP_RGB_LED_TASK_HPP
/*
 * #################################################################################################
 * Build directives
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Define to firmware version
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Includes
 * #################################################################################################
 */

#include <stdint.h>
#include <LedRGB/LedRGB.hpp>
#include "Task.hpp"
#include "stm32h7xx_hal.h"

/*
 * #################################################################################################
 * Data types definition
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Namespace Titanium::Tos
 * #################################################################################################
 */

using namespace Titanium::Tos;
using namespace Titanium::Devices;

/**
 * *************************************************************************************************
 * @brief   Task base class
 * *************************************************************************************************
 * 
 */
class cRgbLedTask: public cTask
{
  public:
    ~cRgbLedTask( void );
    cRgbLedTask( const std::string name, cBytePool *bytePool, cLedRGB* led );

  protected:
    /* Events */
    void main( void ) override final;
    void onStart( void ) override final;
    void onStop( void ) override final;

  private:
    cLedRGB* led;
};

/*
 * #################################################################################################
 * Defines
 * #################################################################################################
 */

/*
 * #################################################################################################
 * External method prototypes
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Data sharing
 * #################################################################################################
 */

#endif /* APP_RGB_LED_TASK_HPP */
