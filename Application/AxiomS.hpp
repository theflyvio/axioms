/**
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @headerfile    : tLayNamCmp.h
 * @brief         : Provide ...
 *
 * @author        : R&D METROVAL Controle de Fluidos.
 * @version       : 1.0
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 METROVAL Controle de Fluidos.
 * All rights reserved.</center></h2>
 *
 * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

/*
 * #################################################################################################
 * Define to prevent recursive inclusion
 * #################################################################################################
 */

#ifndef APP_TAPPLICATION_HPP_
#define APP_TAPPLICATION_HPP_

/*
 * #################################################################################################
 * Build directives
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Define to firmware version
 * #################################################################################################
 */

#define APP_TAPPLICATION_HPP_VER_MAJOR                                                             1
#define APP_TAPPLICATION_HPP_VER_MINOR                                                             0

/*
 * #################################################################################################
 * Includes
 * #################################################################################################
 */

#include <RegisterU08Generic.hpp>
#include <RegisterU08Ranged.hpp>
#include <RegisterUsername.hpp>
#include <RegisterUserid.hpp>
#include <RegisterOne.hpp>
#include <TitaniumLeader.hpp>
#include <Core.hpp>
#include <Board.hpp>
#include <BoardAxiomS.hpp>
#include <BytePool.hpp>
#include <MainTask.hpp>

/*
 * #################################################################################################
 * Data types definition
 * #################################################################################################
 */

using namespace Titanium;
using namespace Titanium::Board;
using namespace Titanium::Tos;

class cAxiomS_t: public cTitaniumLeader {
  private:
    /* Attributes */
    cRegisterU08Generic_t* l1;
    cRegisterU08Ranged_t* l2;
    cRegisterUsername_t* username;
    cRegisterUserid_t* userid;
    //
    cRegisterU08Generic_t* r1;
    cRegisterU08Ranged_t* r2;
    cRegisterUserid_t* rem_userid;
    cRegisterUsername_t* rem_username;
    //
    cRegisterOne_t* one;
  protected:
    /* Methods */
    cBoard* createBoard(void) override;
    void createLocalRegisters(void) override;
    void createRemoteRegisters(void) override;
    void createAppTasks(void) override;
    void startRtosScheduler(void) override;

  public:
      /* OS */
    cBytePool *bytePool;
    /* Tasks */
    /* Main task must be created inside tx_application_define() or any method called from it */
    cMainTask *main_task;
    
    /* Methods */
    cAxiomS_t();
    ~cAxiomS_t();
    void rtosInitCallback(void *unusedMemory);

    cBoardAxiomS* GetBoardAxiom(void){
      return (cBoardAxiomS*)getBoard();
    }
};

/*
 * #################################################################################################
 * Namespace ???
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Defines
 * #################################################################################################
 */

/*
 * #################################################################################################
 * External method prototypes
 * #################################################################################################
 */

/*
 * #################################################################################################
 * Data sharing
 * #################################################################################################
 */

#endif /* APP_TAPPLICATION_HPP_ */
