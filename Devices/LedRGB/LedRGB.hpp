#ifndef DRIVER_LEDRGB_HPP_
#define DRIVER_LEDRGB_HPP_

#include <GPIO.hpp>
#include <PortName.hpp>
#include <Lock.hpp>

namespace Titanium::Devices {

  using namespace Titanium::Drivers;

  class cLedRGB: public cLock {
    private:
      bool ownGPIOs;
      bool off_state;
      cGPIO* gpio_r;
      cGPIO* gpio_g;
      cGPIO* gpio_b;

      uint8_t getLockDependencyCount(void) const override;
      cLock* getLockDependency(uint8_t index) const override;

      cLedRGB(bool ownGPIOs_, cGPIO* r, cGPIO* g, cGPIO* b);

    public:
      cLedRGB(ePortName r_port, uint8_t r_pin, ePortName g_port, uint8_t g_pin, ePortName b_port, uint8_t b_pin);
      cLedRGB(cGPIO* r, cGPIO* g, cGPIO* b);
      ~cLedRGB();

      void set_on_state(bool on_state_);
      void set(const void* const lock, bool r, bool g, bool b);
      void set(const void* const lock, uint8_t v);
  };

}

#endif /* DRIVER_LEDRGB_HPP_ */
