#include "LedRGB.hpp"
#include <Board.hpp>

namespace Titanium::Devices {
  using namespace Titanium::Board;

  cLedRGB::cLedRGB(bool ownGPIOs_, cGPIO* r, cGPIO* g, cGPIO* b)
      : ownGPIOs(ownGPIOs_), off_state(true), gpio_r(r), gpio_g(g), gpio_b(b){
  }

  cLedRGB::cLedRGB(cGPIO* r, cGPIO* g, cGPIO* b)
      : cLedRGB(false, r, g, b){
  }

  cLedRGB::cLedRGB(ePortName r_port, uint8_t r_pin, ePortName g_port, uint8_t g_pin, ePortName b_port, uint8_t b_pin)
      : cLedRGB(true,
          cBoard::getBoard().newGPIO(r_port, r_pin, &cGPIOConfig::defaultOutput),
          cBoard::getBoard().newGPIO(g_port, g_pin, &cGPIOConfig::defaultOutput),
          cBoard::getBoard().newGPIO(b_port, b_pin, &cGPIOConfig::defaultOutput)){
  }

  cLedRGB::~cLedRGB(){
    if(ownGPIOs){
      delete gpio_r;
      delete gpio_g;
      delete gpio_b;
    }
  }

  uint8_t cLedRGB::getLockDependencyCount(void) const{
    return 3;
  }

  cLock* cLedRGB::getLockDependency(uint8_t index) const{
    switch(index){
      case 0:
        return gpio_r;

      case 1:
        return gpio_g;

      case 2:
        return gpio_b;

    }
    throw 0;
  }

  void cLedRGB::set_on_state(bool on_state_){
    off_state = !on_state_;
  }

  void cLedRGB::set(const void* const lock, bool r, bool g, bool b){
    gpio_r->write(lock, r^off_state);
    gpio_g->write(lock, g^off_state);
    gpio_b->write(lock, b^off_state);
  }

  void cLedRGB::set(const void* const lock, uint8_t v){
    gpio_r->write(lock, v&1);
    gpio_g->write(lock, (v>>1)&1);
    gpio_b->write(lock, (v>>2)&1);
  }

}
