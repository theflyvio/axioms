#include <CAT24/CAT24.hpp>

#include <Board.hpp>

namespace Titanium::Devices {

  using namespace Titanium::Board;

  cCAT24::cCAT24(const eCAT24 deviceAddress_, const eI2C i2c_n, cPin* const sda, cPin* const scl, cPin* const wp_)
      : deviceAddress(deviceAddress_), i2c(cBoard::getBoard().getI2C(i2c_n, eI2CSpeedMode::FAST_PLUS, sda, scl)), wp(wp_){
  }

  cCAT24::cCAT24(const eCAT24 deviceAddress_, const eI2C i2c_n, cPin* const sda, cPin* const scl)
      : deviceAddress(deviceAddress_), i2c(cBoard::getBoard().getI2C(i2c_n, eI2CSpeedMode::FAST_PLUS, sda, scl)) {
  }  

  void cCAT24::write(uint32_t address, uint8_t* data, uint32_t len){
    i2c->writeMemory(deviceAddress, address, data, len);
  }

  void cCAT24::read(uint32_t address, uint8_t* data, uint32_t len){
    i2c->readMemory(deviceAddress, address, data, len);
  }

  uint8_t cCAT24::getLockDependencyCount(void) const {
    return 1;
  }

  cLock* cCAT24::getLockDependency(uint8_t index) const {
    return i2c;
  }

}
