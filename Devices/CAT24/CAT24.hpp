#ifndef CAT24M01_CAT24_HPP_
#define CAT24M01_CAT24_HPP_

#include "I2C.hpp"
#include "Lock.hpp"
#include "Pin.hpp"

namespace Titanium::Devices {

  using namespace Titanium::Drivers;

  /**
   *
   *
   *
   */
  class cCAT24: public cLock {
    public:
      static constexpr uint16_t CAT24_WRITE_TIMEOUT = 1000;
      static constexpr uint16_t CAT24_READ_TIMEOUT = 1000;
      
      enum eCAT24 {
        ADDR_0 = 0xA0,  //1010 0000
        ADDR_1 = 0xA4,  //1010 0100
        ADDR_2 = 0xA8,  //1010 1000
        ADDR_3 = 0xAC   //1010 1100
      } deviceAddress;

    private:
      cI2C* i2c;
      cPin* wp;
      uint8_t getLockDependencyCount(void) const override;
      cLock* getLockDependency(uint8_t index) const override;

    protected:
      virtual constexpr uint16_t getPageSize(void) const =0;
      virtual constexpr uint16_t getPageCount(void) const =0;
      virtual constexpr eI2CSpeedMode getMaxFreq(void) const =0;

    public:
      cCAT24(const eCAT24 deviceAddress_, const eI2C i2c_n, cPin* const sda, cPin* const scl, cPin* const wp_);
      cCAT24(const eCAT24 deviceAddress_, const eI2C i2c_n, cPin* const sda, cPin* const scl);

      void write(uint32_t address, uint8_t* data, uint32_t len);
      void read(uint32_t address, uint8_t* data, uint32_t len);
  };

}

#endif /* CAT24M01_CAT24_HPP_ */
