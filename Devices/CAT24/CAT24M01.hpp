/*
 * CAT24M01.hpp
 *
 *  Created on: 31 de dez de 2021
 *      Author: Usuario
 */

#ifndef TITANIUM_DEVICES_CAT24M01_HPP_
#define TITANIUM_DEVICES_CAT24M01_HPP_

#include "CAT24.hpp"
#include "I2C.hpp"
#include "Lock.hpp"
#include "Pin.hpp"


namespace Titanium::Devices {

  using namespace Titanium::Drivers;

  /**
   *
   *
   *
   */
  class cCAT24M01: public cCAT24 {
    protected:
      uint16_t getPageSize(void) const override;
      uint16_t getPageCount(void) const override;
      eI2CSpeedMode getMaxFreq(void) const override;

    public:
      cCAT24M01(const eCAT24 deviceAddress_, const eI2C n, cPin * const sda, cPin * const scl, cPin * const wp);
      cCAT24M01(const eCAT24 deviceAddress_, const eI2C n, cPin * const sda, cPin * const scl);

  };

  inline uint16_t cCAT24M01::getPageSize(void) const {
    return 256;
  }

  inline uint16_t cCAT24M01::getPageCount(void) const {
    return 512;
  }

  inline eI2CSpeedMode cCAT24M01::getMaxFreq(void) const {
    return eI2CSpeedMode::FAST_PLUS;
  }

  inline cCAT24M01::cCAT24M01(const eCAT24 deviceAddress_, const eI2C n, cPin* const sda, cPin* const scl, cPin* const wp) :
      cCAT24(deviceAddress_, n, sda,scl,wp) {
  }

  inline cCAT24M01::cCAT24M01(const eCAT24 deviceAddress_, const eI2C n, cPin* const sda, cPin* const scl) :
      cCAT24(deviceAddress_, n, sda,scl) {
  }  

}

#endif /* TITANIUM_DEVICES_CAT24M01_HPP_ */
