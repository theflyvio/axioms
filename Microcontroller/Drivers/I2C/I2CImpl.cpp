#include "I2CImpl.hpp"

namespace Titanium::Drivers {

  cI2CImpl::cI2CImpl(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_)
      : cI2C(n_, speed_, sda_, scl_){
    #warning verificar validade dos pinos
    //i2c1 scl 
    //
    //i2c4 scl pb6, pb8, ph11, pd12
    //     sda pb7, pb9, ph12, pd13
  }

  void cI2CImpl::config(void) const{
    I2C_HandleTypeDef h;
    h.Instance = getRegBase();
    h.Init.Timing = 0x0050174F;
    h.Init.OwnAddress1 = 0;
    h.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    h.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    h.Init.OwnAddress2 = 0;
    h.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    h.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    h.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if(HAL_I2C_Init( &h)!=HAL_OK){
      throw 0;
    }
    /** Configure Analogue filter
     */
    if(HAL_I2CEx_ConfigAnalogFilter( &h, I2C_ANALOGFILTER_ENABLE)!=HAL_OK){
      throw 0;
    }
    /** Configure Digital filter
     */
    if(HAL_I2CEx_ConfigDigitalFilter( &h, 0)!=HAL_OK){
      throw 0;
    }
    /** I2C Enable Fast Mode Plus
     */
    HAL_I2CEx_EnableFastModePlus(I2C_FASTMODEPLUS_I2C4);
    /*
     * msp init
     */
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
    PeriphClkInitStruct.PeriphClockSelection = getPeriphClockSelection();
    switch(getN()){
      case eI2C::I2C_1:
      case eI2C::I2C_2:
      case eI2C::I2C_3:
        PeriphClkInitStruct.I2c123ClockSelection = 0;
        #warning missing configuration
        throw 0;
      break;

      case eI2C::I2C_4:
        PeriphClkInitStruct.I2c4ClockSelection = RCC_I2C4CLKSOURCE_D3PCLK1;
      break;
    }
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
      throw 0;
    }
    sda->config(ePinMode::pmALTERNATE, ePinOutputType::OpenDrain, ePinPull::NO_PULL, ePinSpeed::LOW, getAlternateFunction());
    scl->config(ePinMode::pmALTERNATE, ePinOutputType::OpenDrain, ePinPull::NO_PULL, ePinSpeed::LOW, getAlternateFunction());
    switch(getN()){
      case eI2C::I2C_1:
        __HAL_RCC_I2C1_CLK_ENABLE();
      break;

      case eI2C::I2C_2:
        __HAL_RCC_I2C2_CLK_ENABLE();
      break;

      case eI2C::I2C_3:
        __HAL_RCC_I2C3_CLK_ENABLE();
      break;

      case eI2C::I2C_4:
        __HAL_RCC_I2C4_CLK_ENABLE();
      break;
    }
  }
  
  void cI2CImpl::writeMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const {

    HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
  }

  void cI2CImpl::readMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const {

    HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
  }

  // void HAL_I2C_MspDeInit(I2C_HandleTypeDef* i2cHandle){

  //   if(i2cHandle->Instance==I2C4){
  //     /* USER CODE BEGIN I2C4_MspDeInit 0 */

  //     /* USER CODE END I2C4_MspDeInit 0 */
  //     /* Peripheral clock disable */
  //     __HAL_RCC_I2C4_CLK_DISABLE();

  //     /**I2C4 GPIO Configuration
  //      PD12     ------> I2C4_SCL
  //      PD13     ------> I2C4_SDA
  //      */
  //     HAL_GPIO_DeInit(GPIOD, GPIO_PIN_12);

  //     HAL_GPIO_DeInit(GPIOD, GPIO_PIN_13);

  //     /* USER CODE BEGIN I2C4_MspDeInit 1 */

  //     /* USER CODE END I2C4_MspDeInit 1 */
  //   }
  // }

  uint32_t cI2CImpl::getPeriphClockSelection(void) const{
    uint32_t v[] = {RCC_PERIPHCLK_I2C123, RCC_PERIPHCLK_I2C123, RCC_PERIPHCLK_I2C123, RCC_PERIPHCLK_I2C4};
    return v[getN()];
  }

  uint32_t cI2CImpl::getAlternateFunction(void) const{
    switch(getN()){
      case eI2C::I2C_1:
        return GPIO_AF4_I2C1;
        
      case eI2C::I2C_2:
        return GPIO_AF4_I2C2;
        
      case eI2C::I2C_3:
        return GPIO_AF4_I2C3;
        
      default:
        return GPIO_AF4_I2C4;
    }
  }

  I2C_TypeDef* cI2CImpl::getRegBase(void) const{
    I2C_TypeDef* v[] = {I2C1, I2C2, I2C3, I2C4};
    return v[getN()];
  }

  //uint32_t cI2CImpl::getClockSource(void) const{
  //  uint32_t v[] = {LL_RCC_I2C123_CLKSOURCE_PCLK1, LL_RCC_I2C123_CLKSOURCE_PCLK1, LL_RCC_I2C123_CLKSOURCE_PCLK1, LL_RCC_I2C4_CLKSOURCE_PCLK4};
  //  return v[getN()];
  //}

}
