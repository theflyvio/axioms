#ifndef TITANIUM_DRIVERS_I2C_I2CIMPL_HPP_
#define TITANIUM_DRIVERS_I2C_I2CIMPL_HPP_

#include <I2C.hpp>

#include "stm32h7xx_hal.h"

namespace Titanium::Drivers {

  class cI2CImpl: public cI2C {
    private:
      I2C_HandleTypeDef hi2c;

      void config(void) const override;

      uint32_t getPeriphClockSelection(void) const;
      uint32_t getAlternateFunction(void) const;
      //uint32_t getClockSource(void) const;
      I2C_TypeDef* getRegBase(void) const;

    public:
      cI2CImpl(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_);

      void writeMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const override;
      void readMemory(uint16_t deviceAddress, uint16_t address, uint8_t* data, uint32_t len) const override;
  };

}

#endif /* TITANIUM_DRIVERS_I2C_I2CIMPL_HPP_ */
