#include <PortImpl.hpp>

#include <PinImpl.hpp>
#include "stm32h7xx_hal.h"

namespace Titanium::Drivers {

  cPortImpl::cPortImpl(ePortName portName_, GPIO_TypeDef* regBase_)
      : cPort(portName_), regBase(regBase_){
    switch(portName_){
      case ePortName::A:
        __HAL_RCC_GPIOA_CLK_ENABLE();
      break;

      case ePortName::B:
        __HAL_RCC_GPIOB_CLK_ENABLE();
      break;

      case ePortName::C:
        __HAL_RCC_GPIOC_CLK_ENABLE();
      break;

      case ePortName::D:
        __HAL_RCC_GPIOD_CLK_ENABLE();
      break;

      case ePortName::E:
        __HAL_RCC_GPIOE_CLK_ENABLE();
      break;

      case ePortName::F:
        __HAL_RCC_GPIOF_CLK_ENABLE();
      break;

      case ePortName::G:
        __HAL_RCC_GPIOG_CLK_ENABLE();
      break;

      case ePortName::H:
        __HAL_RCC_GPIOH_CLK_ENABLE();
      break;

      case ePortName::I:
        __HAL_RCC_GPIOI_CLK_ENABLE();
      break;

      case ePortName::J:
        __HAL_RCC_GPIOJ_CLK_ENABLE();
      break;

      case ePortName::K:
        __HAL_RCC_GPIOK_CLK_ENABLE();
      break;

    }
  }

  cPin* cPortImpl::createPin(const uint8_t index) const{
    return new cPinImpl(this, index);
  }

}
