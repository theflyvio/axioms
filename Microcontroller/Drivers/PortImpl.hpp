#ifndef TITANIUM_DRIVER_TPORTIMPL_HPP_
#define TITANIUM_DRIVER_TPORTIMPL_HPP_

#include <Port.hpp>
#include <stm32h7xx.h>

namespace Titanium::Drivers {

  class PACKED cPortImpl: public cPort {
    private:
      GPIO_TypeDef* regBase;

      cPin* createPin(const uint8_t index) const override;

    public:
      cPortImpl(ePortName portName_, GPIO_TypeDef* regBase_);

      friend class cPinImpl;
  };

}

#endif /* TITANIUM_DRIVER_TPORTIMPL_HPP_ */
