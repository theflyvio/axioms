#include <PinImpl.hpp>

#include "stm32h7xx_hal.h"
#include <Port.hpp>

namespace Titanium::Drivers {

  void cPinImpl::config(ePinMode const mode, ePinOutputType const outputType, ePinPull const pull,
      ePinSpeed const speed, uint32_t alternate) const{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = getPinMask();
    GPIO_InitStruct.Mode = getMode(mode)|getOutputType(outputType);
    GPIO_InitStruct.Pull = getPull(pull);
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = alternate;
    HAL_GPIO_Init(getRegBase(), &GPIO_InitStruct);
  }

  uint8_t cPinImpl::read(void) const{
#warning pode ler um pino sem travar?
    return HAL_GPIO_ReadPin(getRegBase(), getPinMask())>0;
  }

  void cPinImpl::write(const void* const lock, bool const value) const{
    grantLocked(lock);
    HAL_GPIO_WritePin(getRegBase(), getPinMask(), value ? GPIO_PIN_SET : GPIO_PIN_RESET);
  }

  void cPinImpl::toggle(const void* const lock) const{
    grantLocked(lock);
    HAL_GPIO_TogglePin(getRegBase(), getPinMask());
  }

  uint32_t cPinImpl::getMode(ePinMode const mode) const{
    uint32_t r;
    switch(mode){
      case ePinMode::pmINPUT:
        r = MODE_INPUT;
      break;

      case ePinMode::pmOUTPUT:
        r = MODE_OUTPUT;
      break;

      case ePinMode::pmALTERNATE:
        r = MODE_AF;
      break;

      case ePinMode::pmANALOG:
        r = MODE_ANALOG;
      break;

      default:
        throw 0;
      break;
    }
    return r;
  }

  uint32_t cPinImpl::getOutputType(ePinOutputType const outputType) const{
    uint32_t r;
    switch(outputType){
      case ePinOutputType::OpenDrain:
        r = OUTPUT_OD;
      break;

      case ePinOutputType::PushPull:
        r = OUTPUT_PP;
      break;

      default:
        throw 0;
      break;
    }
    return r;
  }

  uint32_t cPinImpl::getPull(ePinPull const pull) const{
    uint32_t r;
    switch(pull){
      case ePinPull::NO_PULL:
        r = GPIO_NOPULL;
      break;

      case ePinPull::PULL_UP:
        r = GPIO_PULLUP;
      break;

      case ePinPull::PULL_DOWN:
        r = GPIO_PULLDOWN;
      break;

      default:
        throw 0;
      break;
    }
    return r;
  }

  uint32_t cPinImpl::getSpeed(ePinSpeed const speed) const{
    uint32_t r;
    switch(speed){
      case ePinSpeed::LOW:
        r = GPIO_SPEED_FREQ_LOW;
      break;

      case ePinSpeed::MEDIUM:
        r = GPIO_SPEED_FREQ_MEDIUM;
      break;

      case ePinSpeed::HIGH:
        r = GPIO_SPEED_FREQ_HIGH;
      break;

      case ePinSpeed::VERY_HIGH:
        r = GPIO_SPEED_FREQ_VERY_HIGH;
      break;

      default:
        throw 0;
      break;
    }
    return r;
  }

}
