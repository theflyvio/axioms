#include "CoreImpl.hpp"

#include "main.hpp"

#include "stm32h7xx_hal.h"
#include "GPIOImpl.hpp"
#include "PortImpl.hpp"
#include "I2CImpl.hpp"

#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0         ((uint32_t)0x00000007) /*!< 0 bit  for pre-emption priority,
                                                                 4 bits for subpriority */
#define NVIC_PRIORITYGROUP_1         ((uint32_t)0x00000006) /*!< 1 bit  for pre-emption priority,
                                                                 3 bits for subpriority */
#define NVIC_PRIORITYGROUP_2         ((uint32_t)0x00000005) /*!< 2 bits for pre-emption priority,
                                                                 2 bits for subpriority */
#define NVIC_PRIORITYGROUP_3         ((uint32_t)0x00000004) /*!< 3 bits for pre-emption priority,
                                                                 1 bit  for subpriority */
#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) /*!< 4 bits for pre-emption priority,
                                                                 0 bit  for subpriority */
#endif

void SystemClock_Config(void);

namespace Titanium::Drivers {

  void cCoreImpl::onInit(void){
    HAL_Init();
    SystemClock_Config();
    /* Update the time base */
    if(HAL_InitTick(TICK_INT_PRIORITY)!=HAL_OK){
      Error_Handler();
    }
  }

  cPort* cCoreImpl::newPort(ePortName const pn) const{
    cPort* port;
    switch(pn){
      case ePortName::A:
        port = new cPortImpl(pn, GPIOA);
        port->init(0b1111111111111111);
      break;

      case ePortName::B:
        port = new cPortImpl(pn, GPIOB);
        port->init(0b1111111111111111);
      break;

      case ePortName::C:
        port = new cPortImpl(pn, GPIOC);
        port->init(0b1111111111111111);
      break;

      case ePortName::D:
        port = new cPortImpl(pn, GPIOD);
        port->init(0b1111111111111111);
      break;

      case ePortName::E:
        port = new cPortImpl(pn, GPIOE);
        port->init(0b1111111111111111);
      break;

      case ePortName::F:
        port = new cPortImpl(pn, GPIOF);
        port->init(0b1111111111111111);
      break;

      case ePortName::G:
        port = new cPortImpl(pn, GPIOG);
        port->init(0b1111111111111111);
      break;

      case ePortName::H:
        port = new cPortImpl(pn, GPIOH);
        port->init(0b1111111111111111);
      break;

      case ePortName::I:
        port = new cPortImpl(pn, GPIOI);
        port->init(0b0000111111111111);
      break;

      default:
        port = nullptr;
      break;
    }
    return port;
  }

  cGPIO* cCoreImpl::newGPIO(cPin* const pin, const cGPIOConfig* const config) const{
    return new cGPIOImpl(pin, config);
  }

  cI2C* cCoreImpl::newI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_) const{
    return new cI2CImpl(n_, speed_, sda_, scl_);
  }

}

void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE0);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim){
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if(htim->Instance==TIM1){
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}
