#ifndef DRIVERS_PINIMPL_HPP_
#define DRIVERS_PINIMPL_HPP_

#include <Pin.hpp>
#include <PortImpl.hpp>

namespace Titanium::Drivers {

  class cPinImpl: public cPin {
    private:
      uint32_t getMode(ePinMode const mode) const;
      uint32_t getOutputType(ePinOutputType const outputType) const;
      uint32_t getPull(ePinPull const pull) const;
      uint32_t getSpeed(ePinSpeed const speed) const;
      GPIO_TypeDef* getRegBase(void) const;

    public:
      inline cPinImpl(const cPort* port_, const uint8_t pin_)
          : cPin(port_, pin_){
      }

      void config(ePinMode const mode, ePinOutputType const outputType, ePinPull const pull,
          ePinSpeed const speed, uint32_t alternate) const override;
      uint8_t read(void) const override;
      void write(const void* const lock, const bool value) const override;
      void toggle(const void* const lock) const override;

  };

  inline GPIO_TypeDef* cPinImpl::getRegBase(void) const{
    return ((const cPortImpl*)getPort())->regBase;
  }

}

#endif /* DRIVERS_PINIMPL_HPP_ */
