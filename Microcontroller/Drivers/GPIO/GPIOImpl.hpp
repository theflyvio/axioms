#ifndef DRIVER_CORE_GPIO_GPIOIMPL_HPP_
#define DRIVER_CORE_GPIO_GPIOIMPL_HPP_

#include <GPIO.hpp>

namespace Titanium::Drivers {
  //class cCoreImpl;

  class cGPIOImpl: public cGPIO {
    protected:
      cGPIOImpl(cPin* const pin, const cGPIOConfig* config_);
      virtual ~cGPIOImpl();

    public:

      friend class cCoreImpl;
  };

  inline cGPIOImpl::~cGPIOImpl(){
  }

}

#endif /* DRIVER_CORE_GPIO_GPIOIMPL_HPP_ */
