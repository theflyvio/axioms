#include "GPIOImpl.hpp"

#include <Core.hpp>

namespace Titanium::Drivers {

  cGPIOImpl::cGPIOImpl(cPin* const pin, const cGPIOConfig* const config_)
      : cGPIO(pin, config_){
  }

}
