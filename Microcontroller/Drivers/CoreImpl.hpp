#ifndef TITANIUM_DRIVERS_COREIMPL_HPP_
#define TITANIUM_DRIVERS_COREIMPL_HPP_

#include <Core.hpp>
#include <GPIO.hpp>

namespace Titanium::Drivers {

#define NAND_SPI_SCK_Pin GPIO_PIN_2
#define NAND_SPI_SCK_GPIO_Port GPIOE
#define NAND_SPI_nCS0_Pin GPIO_PIN_3
#define NAND_SPI_nCS0_GPIO_Port GPIOE
#define NAND_SPI_nCS1_Pin GPIO_PIN_4
#define NAND_SPI_nCS1_GPIO_Port GPIOE
#define NAND_SPI_MISO_Pin GPIO_PIN_5
#define NAND_SPI_MISO_GPIO_Port GPIOE
#define NAND_SPI_MOSI_Pin GPIO_PIN_6
#define NAND_SPI_MOSI_GPIO_Port GPIOE
#define NAND_SPI_nCS2_Pin GPIO_PIN_8
#define NAND_SPI_nCS2_GPIO_Port GPIOI
#define NAND_SPI_nCS3_Pin GPIO_PIN_13
#define NAND_SPI_nCS3_GPIO_Port GPIOC
#define NOR_SPI_IO3_Pin GPIO_PIN_6
#define NOR_SPI_IO3_GPIO_Port GPIOF
#define NOR_SPI_IO2_Pin GPIO_PIN_7
#define NOR_SPI_IO2_GPIO_Port GPIOF
#define NOR_SPI_IO0_Pin GPIO_PIN_8
#define NOR_SPI_IO0_GPIO_Port GPIOF
#define NOR_SPI_IO1_Pin GPIO_PIN_9
#define NOR_SPI_IO1_GPIO_Port GPIOF
#define NOR_SPI_SCK_Pin GPIO_PIN_2
#define NOR_SPI_SCK_GPIO_Port GPIOB
#define NOR_SPI_nCS0_Pin GPIO_PIN_6
#define NOR_SPI_nCS0_GPIO_Port GPIOB

  class cCoreImpl: public cCore {
    private:
      void onInit(void) override;

      cPort* newPort(ePortName const p) const override;
      cGPIO* newGPIO(cPin* const pin, const cGPIOConfig* const config) const override;
      cI2C* newI2C(const eI2C n_, const eI2CSpeedMode speed_, cPin* const sda_, cPin* const scl_) const override;

  };

}

#endif
